import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../services/data.service';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() sidebarActive: boolean;


  constructor(private router: Router, private _dataService: DataService, private _authService: AuthService) {
  }

  ngOnInit() {
  }

  signOut() {
    this._authService.logout();
    this.router.navigate(['/login']);
  }
}
