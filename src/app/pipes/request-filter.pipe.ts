import {Pipe, PipeTransform} from '@angular/core';
import {DataService} from '../services/data.service';

@Pipe({
  name: 'requestFilter'
})
export class RequestFilterPipe implements PipeTransform {
  constructor(private _dataService: DataService) {
  }

  transform(value: any, args?: any): any {
    if (!value && !args) {
      return value;
    }
    if (typeof args.status !== 'undefined' && args.status !== -1) {
      value = value.filter(item => item.status === args.status);
    }
    if (typeof args.searchWord !== 'undefined' && args.searchWord !== '') {
      value = value.filter(item => item.subject.indexOf(args.searchWord) !== -1);
    }
    if (typeof args.showRequestsToMe !== 'undefined' && args.showRequestsToMe && this._dataService.currentUser !== null) {
      if (!args.showRequestsFromMe) {
        value = value.filter(item => item.receiver.uid === this._dataService.currentUser.uid);
      }
    }
    if (typeof args.showRequestsFromMe !== 'undefined' && args.showRequestsFromMe && this._dataService.currentUser !== null) {
      if (!args.showRequestsToMe) {
        value = value.filter(item => item.sender.uid === this._dataService.currentUser.uid);
      }
    }
    return value;
  }
}
