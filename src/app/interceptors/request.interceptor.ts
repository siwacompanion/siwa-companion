import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, Subject, throwError} from 'rxjs';
import {map, catchError, switchMap} from 'rxjs/operators';
import {NotificationService} from '../services/notification.service';
import {Token} from '../models/token';
import {DataService} from '../services/data.service';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  private _refreshSubject: Subject<any> = new Subject<any>();

  constructor(
    private _notificationService: NotificationService,
    private _dataService: DataService,
    private _router: Router,
    private _authService: AuthService
  ) {
  }

  private static _updateHeader(req) {
    const token = new Token().deserialize(JSON.parse(localStorage.getItem('_token')));
    req = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${token.access_token}`)
    });
    return req;
  }

  private _ifTokenExpired() {
    this._refreshSubject.subscribe({
      complete: () => {
        this._refreshSubject = new Subject<any>();
      }
    });
    if (this._refreshSubject.observers.length === 1) {
      this._authService.refreshToken().subscribe(this._refreshSubject);
    }
    return this._refreshSubject;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.endsWith('/token/refresh')) {
      return next.handle(request);
    } else {
      if (request.withCredentials) {
        request = RequestInterceptor._updateHeader(request);
      }
      this._notificationService.notifyRequestStarted();
      return next.handle(request)
        .pipe(
          map(data => {
            if (data instanceof HttpResponse) {
              this._notificationService.notifyRequestCompleted();
            }
            return data;
          }),
          catchError(error => {
            if (error instanceof HttpErrorResponse) {
              let errorMessage = '';

              if (error.status === 401) {
                if (error.error.message === 'Invalid Access Token - token not found.') {
                  return this._ifTokenExpired().pipe(
                    switchMap(() => {
                      return next.handle(RequestInterceptor._updateHeader(request))
                        .pipe(
                          map(data => {
                            if (data instanceof HttpResponse) {
                              this._notificationService.notifyRequestCompleted();
                            }
                            return data;
                          }),
                          catchError(refreshError => {
                            this._dataService.currentUser = null;
                            this._dataService.token = null;
                            this._router.navigate([`/login`]);
                            return throwError(refreshError);
                          }));
                    })
                  );
                }
              } else {
                errorMessage = 'Oops, something went wrong...';
              }
              this._notificationService.notifyError(errorMessage);
            }
            return throwError(error);
          })
        );
    }
  }
}
