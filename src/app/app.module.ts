import {AppRoutingModule} from './app-routing/app-routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {enableProdMode, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {HelpcenterComponent} from './helpcenter/helpcenter.component';
import {SettingsComponent} from './settings/settings.component';
import {SidebarProfileComponent} from './sidebar-profile/sidebar-profile.component';
import {ProfileComponent} from './profile/profile.component';
import {UserItemComponent} from './user-item/user-item.component';
import {LoadingComponent} from './loading/loading.component';
import {LoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {StompConfig, StompService} from '@stomp/ng2-stompjs';
import {AccordionModule, BsDropdownModule, ModalModule} from 'ngx-bootstrap';
import {CKEditorModule} from 'ng2-ckeditor';
import { RequestsComponent } from './requests/requests.component';
import { RequestFilterPipe } from './pipes/request-filter.pipe';
import {RequestInterceptor} from './interceptors/request.interceptor';

enableProdMode();

const stompConfig: StompConfig = {
  // Which server?
  url: 'ws://localhost:8080/ws',

  // Headers
  // Typical keys: login, passcode, host
  headers: {
    login: 'guest',
    passcode: 'guest'
  },

  // How often to heartbeat?
  // Interval in milliseconds, set to 0 to disable
  heartbeat_in: 20000, // Typical value 0 - disabled
  heartbeat_out: 0, // Typical value 20000 - every 20 seconds

  // Wait in milliseconds before attempting auto reconnect
  // Set to 0 to disable
  // Typical value 5000 (5 seconds)
  reconnect_delay: 5000,

  // Will log diagnostics on console
  debug: false
};

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HelpcenterComponent,
    SettingsComponent,
    SidebarProfileComponent,
    ProfileComponent,
    UserItemComponent,
    LoadingComponent,
    LoginComponent,
    RequestsComponent,
    RequestFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    CKEditorModule,
    FormsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
    StompService,
    {
      provide: StompConfig,
      useValue: stompConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
