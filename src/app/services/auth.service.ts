import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {DataService} from './data.service';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Token} from '../models/token';
import {User} from '../models/user';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private _dataService: DataService, private _http: HttpClient, private _router: Router, private _notificationService: NotificationService) {
  }

  public refreshToken() {
    return new Observable(observer => {
      const existingTokens = this._dataService.token;
      if (existingTokens.refresh_token) {
        this._http.get(this._dataService.apiUrl + '/token/refresh', {
          headers: {'Authorization': 'Bearer ' + existingTokens.refresh_token},
        }).subscribe(data => {
            const token = new Token().deserialize(data);
            localStorage.setItem('_token', JSON.stringify(token));
            observer.next(token);
          },
          () => {
            this._dataService.currentUser = null;
            this._dataService.token = null;
            this._router.navigate([`/login`]).then(
              () => {
                this._notificationService.notifyRequestCompleted();
              }
            );
          });
      }
    });
  }

  public login(username: string, password: string) {
    return new Promise((resolve, reject) => {
      const credentials = username + ':' + password;
      const encodedCryptedCredentials = btoa(credentials);

      this._http.get(this._dataService.apiUrl + '/token', {
        headers: {'Authorization': 'Basic ' + encodedCryptedCredentials},
      })
        .toPromise()
        .then(
          data => {
            const token = new Token().deserialize(data);
            localStorage.setItem('_token', JSON.stringify(token));
            this._http.get(this._dataService.apiUrl + '/users/me', {
              withCredentials: true
            }).subscribe(
              (user: User) => {
                this._dataService.currentUser = new User().deserialize(user);
                resolve(user);
              },
              (error: HttpErrorResponse) => {
                reject(error);
              }
            );
          }
        ).catch(
        error => {
          reject(error);
        }
      );
    });
  }

  public logout() {
    if (this._dataService.currentUser) {
      this._dataService.currentUser.status = this._dataService.statuses.find(status => status.uid === 2);
      this._dataService.put('/users/me', JSON.stringify(this._dataService.currentUser), true).then(
        () => {
          this._dataService.currentUser = null;
          this._dataService.token = null;
          this._router.navigate([`/login`]);
        }
      );
    } else {
      this._dataService.token = null;
      this._router.navigate([`/login`]);
    }
  }
}
