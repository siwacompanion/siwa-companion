import { Injectable } from '@angular/core';
import {BsModalService} from 'ngx-bootstrap';
import {DataService} from './data.service';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  constructor(private modalService: BsModalService, private _dataService: DataService) { }

  show(content, config) {
    this._dataService.modalReference = this.modalService.show(content, config);
  }
}
