import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  onRequestStarted: Subject<void> = new Subject<void>();
  onRequestCompleted: Subject<void> = new Subject<void>();
  onError: Subject<string> = new Subject<string>();
  onOverviewGridsLoaded: Subject<void> = new Subject<void>();
  onHandleRequestDeepLink: Subject<void> = new Subject<void>();

  overviewGridsLoaded = false;

  constructor() {
  }

  public notifyRequestStarted(): void {
    this.onRequestStarted.next();
  }

  public notifyRequestCompleted(): void {
    this.onRequestCompleted.next();
  }

  public notifyError(errorMessage: string): void {
    this.onError.next(errorMessage);
  }

  public notifyOverviewGridsLoaded(): void {
    this.overviewGridsLoaded = true;
    this.onOverviewGridsLoaded.next();
  }

  public notifyHandleRequestDeepLink(): void {
    this.onHandleRequestDeepLink.next();
  }
}
