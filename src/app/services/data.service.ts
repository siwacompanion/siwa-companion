import {EventEmitter, Injectable, Output} from '@angular/core';
import {User} from '../models/user';
import {Token} from '../models/token';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Status} from '../models/status';
import {Team} from '../models/team';
import {StompService} from '@stomp/ng2-stompjs';
import {BsModalRef} from 'ngx-bootstrap';
import {Request} from '../models/request';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  @Output() changeUser: EventEmitter<User> = new EventEmitter();

  private _currentUser;
  private _token: Token;
  private readonly _apiUrl: string;
  private readonly _hostUrl: string;
  private _statuses: Array<Status> = [];
  private _teams: Array<Team> = [];
  private _requests: Array<Request> = [];
  private _teamSubscription;
  private _loading = 0;
  private _modalReference: BsModalRef;

  constructor(private _http: HttpClient, private _stompService: StompService) {
    if (window.location.origin.includes('localhost')) {
      this._hostUrl = 'http://localhost';
      this._apiUrl = 'http://localhost/rest';
    } else {
      this._hostUrl = window.location.origin;
      this._apiUrl = window.location.origin;
    }
  }

  get hostUrl(): string {
    return this._hostUrl;
  }

  get apiUrl(): string {
    return this._apiUrl;
  }

  get currentUser(): User {
    if (!this._currentUser) {
      if (localStorage.getItem('_currentUser')) {
        this._currentUser = new User().deserialize(JSON.parse(localStorage.getItem('_currentUser')));
        this.changeUser.emit(this._currentUser);
      }
    }
    return this._currentUser;
  }

  set currentUser(user: User) {
    this._currentUser = user;
    this.changeUser.emit(this._currentUser);
    if (user === null) {
      localStorage.removeItem('_currentUser');
      return;
    }
    localStorage.setItem('_currentUser', JSON.stringify(this._currentUser));
  }

  get token() {
    if (!this._token) {
      this._token = new Token().deserialize(JSON.parse(localStorage.getItem('_token')));
    }
    return this._token;
  }

  set token(token: Token) {
    this._token = token;

    if (token === null) {
      localStorage.removeItem('_token');
      return;
    }
    localStorage.setItem('_token', JSON.stringify(this._token));
  }

  public get(route: string, withCredentials = false) {
    return new Promise((resolve, reject) => {
      this._http.get(this.apiUrl + route, {
        withCredentials: withCredentials
      }).subscribe(
        data => {
          resolve(data);
        },
        (error: HttpErrorResponse) => {
          reject(error);
        }
      );
    });
  }

  public put(route: string, data: any, withCredentials = false) {
    return new Promise((resolve, reject) => {
      this._http.put(this.apiUrl + route, data, {
        withCredentials: withCredentials
      }).subscribe(
        (response: any) => {
          resolve(response);
        },
        (error: HttpErrorResponse) => {
          reject(error);
        }
      );
    });
  }

  public reloadMe() {
    return new Promise((resolve, reject) => {
      this.get('/users/me', true).then(
        (user: User) => {
          this.currentUser = new User().deserialize(user);
          resolve(user);
        },
        (error: HttpErrorResponse) => {
          reject(error);
        }
      );
    });
  }

  get statuses(): Array<Status> {
    return this._statuses;
  }

  set statuses(value: Array<Status>) {
    this._statuses = value;
  }

  get teams(): Array<Team> {
    return this._teams;
  }

  set teams(value: Array<Team>) {
    this._teams = value;
  }

  get requests(): Array<Request> {
    return this._requests;
  }

  set requests(value: Array<Request>) {
    this._requests = value;
  }

  get loading(): number {
    return this._loading;
  }

  set loading(value: number) {
    this._loading = value;
  }

  get modalReference(): BsModalRef {
    return this._modalReference;
  }

  set modalReference(value: BsModalRef) {
    this._modalReference = value;
  }

  loadTeams() {
    this.get('/teams', true).then(
      (teams: Array<Team>) => {
        this.teams = [];
        teams.forEach(team => {
          this._teams.push(new Team().deserialize(team));
        });
      }
    );
  }

  loadRequests() {
    this.loading++;
    this.get('/requests', true).then(
      (requests: Array<Request>) => {
        this.requests = [];
        requests.forEach(request => {
          this._requests.push(new Request().deserialize(request));
        });
        this.loading--;
      }
    );
  }

  subscribeToTeams() {
    this._teamSubscription = this._stompService.subscribe('/exchange/teams');
    this._teamSubscription.map((message: any) => {
      return message.body;
    }).subscribe((msg_body: string) => {
      this.teams = [];
      const teams = JSON.parse(msg_body);
      teams.forEach(team => {
        this._teams.push(new Team().deserialize(team));
      });
    });
  }

  unsubscribeFromTeams() {
  }

  loadStatuses() {
    this.loading++;
    this.get('/users/statuses').then(
      (statuses: Array<Status>) => {
        if (statuses.length > 0) {
          this.statuses = [];
          statuses.forEach(status => {
            this._statuses.push(new Status().deserialize(status));
          });
        }
        this.loading--;
      }
    );
  }
}
