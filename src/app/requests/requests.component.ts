import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
  public showRequestsToMe = true;
  public showRequestsFromMe = true;
  public showByStatus = -1;
  public searchWord = '';

  constructor(public dataService: DataService) {
    this.dataService.loadRequests();
  }

  ngOnInit() {
  }

}
