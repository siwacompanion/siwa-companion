import {Component, Input} from '@angular/core';
import {User} from '../models/user';
import {DomSanitizer} from '@angular/platform-browser';
import {DataService} from '../services/data.service';
import {ModalService} from '../services/modal.service';

declare var CKEDITOR;

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.scss']
})
export class UserItemComponent {
  @Input() user: User;
  public editorValue = '<p>JavaScript code:</p>\n' +
    '\n' +
    '<pre>\n' +
    '<code class="language-javascript">var cow = new Mammal( "moo", {\n' +
    '\tlegs: 4\n' +
    '} );</code></pre>\n' +
    '\n' +
    '<p>Unknown markup:</p>\n' +
    '\n' +
    '<pre>\n' +
    '<code> ________________\n' +
    '/                \\\n' +
    '| How about moo? |  ^__^\n' +
    '\\________________/  (oo)\\_______\n' +
    '                  \\ (__)\\       )\\/\\\n' +
    '                        ||----w |\n' +
    '                        ||     ||\n' +
    '</code></pre>\n';
  constructor(private sanitizer: DomSanitizer, public dataService: DataService, public modalService: ModalService) {
  }
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
