import {Component, OnInit} from '@angular/core';
import {Status} from '../models/status';
import {User} from '../models/user';
import {DataService} from '../services/data.service';
import {ModalService} from '../services/modal.service';

@Component({
  selector: 'app-sidebar-profile',
  templateUrl: './sidebar-profile.component.html',
  styleUrls: ['./sidebar-profile.component.scss']
})
export class SidebarProfileComponent {
  statuses: Array<Status> = [];
  selectedStatus: Status;
  user: User;

  constructor(public dataService: DataService, public modalService: ModalService) {
    this.user = this.dataService.currentUser;
    this.dataService.changeUser.subscribe(user => {
      this.user = user;
    });
    this.statuses = this.dataService.statuses;
  }
  selectStatus(status: Status) {
    if (this.user.status.uid !== status.uid) {
      this.dataService.modalReference.hide();
      this.user.status = status;
      this.dataService.put('/users/me', JSON.stringify(this.user), true).then(
        (data) => {
          this.dataService.currentUser = new User().deserialize(data);
        }
      );
    } else {
      this.dataService.modalReference.hide();
    }
  }
}
