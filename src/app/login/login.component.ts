import {Component, DoCheck, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {User} from '../models/user';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, DoCheck {
  loginForm: FormGroup;
  redirectTo: string;

  constructor(private _router: Router, private _route: ActivatedRoute, private formBuilder: FormBuilder, private _authService: AuthService, private _dataService: DataService) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngDoCheck() {
    this.redirectTo = this._route.snapshot.queryParams['redirectTo'] || '/';
  }

  createForm(): void {
    this.loginForm = this.formBuilder.group(
      {
        username: ['', Validators.required],
        password: ['', Validators.required],
      }
    );
  }

  signIn() {
    const username: AbstractControl = this.loginForm.get('username');
    const password: AbstractControl = this.loginForm.get('password');

    if (this.loginForm.invalid) {
      username.markAsTouched();
      password.markAsTouched();
      return;
    }
    this._authService.login(username.value, password.value).then(
      (user: User) => {
        this._dataService.currentUser.status = this._dataService.statuses.find(status => status.uid === 1);
        this._dataService.put('/users/me', JSON.stringify(this._dataService.currentUser), true).then(
          (data) => {
            this._dataService.currentUser = new User().deserialize(data);
          }
        );
        this._router.navigate([`/${this.redirectTo}`]);
      },
      error => {
      }
    );
  }
}
