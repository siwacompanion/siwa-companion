import {HelpcenterComponent} from './../helpcenter/helpcenter.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SettingsComponent} from '../settings/settings.component';
import {ProfileComponent} from '../profile/profile.component';
import {LoginComponent} from '../login/login.component';
import {AuthGuard} from '../guards/auth.guard';
import {RequestsComponent} from '../requests/requests.component';

const routes: Routes = [
  {path: '', redirectTo: 'helpcenter', pathMatch: 'full', data: {title: 'SIWA Companion'}, canActivate: [AuthGuard]},
  {path: 'helpcenter', component: HelpcenterComponent, data: {title: 'SIWA Helpcenter'}, canActivate: [AuthGuard]},
  {path: 'requests', component: RequestsComponent, data: {title: 'Requests'}, canActivate: [AuthGuard]},
  {path: 'settings', component: SettingsComponent, data: {title: 'Einstellungen'}, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent, data: {title: 'Mein Profil'}, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent, data: {title: 'Login', sidebarHidden: true}, canActivate: [AuthGuard]},
  {path: 'login/:redirectTo', component: LoginComponent, data: {title: 'Login', sidebarHidden: true}, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes, {
        enableTracing: false,
        useHash: true
      })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
