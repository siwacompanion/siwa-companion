import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private _authService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (state.url.startsWith('/login') && localStorage.getItem('_token')) {
      return false;
    }

    if (localStorage.getItem('_token')) {
      return true;
    }

    if (!state.url.startsWith('/login')) {
      this._authService.logout();
      this.router.navigate(['/login'], {queryParams: {redirectTo: state.url}});
    }

    return true;
  }
}
