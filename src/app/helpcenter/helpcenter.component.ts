import {Component, OnDestroy} from '@angular/core';
import {DataService} from '../services/data.service';
import 'rxjs-compat/add/operator/map';

@Component({
  selector: 'app-helpcenter',
  templateUrl: './helpcenter.component.html',
  styleUrls: ['./helpcenter.component.scss']
})
export class HelpcenterComponent implements OnDestroy {
  constructor(public dataService: DataService) {
    this.dataService.loadTeams();
    this.dataService.subscribeToTeams();
  }
  ngOnDestroy() {
    this.dataService.unsubscribeFromTeams();
  }
}
