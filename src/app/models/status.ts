export class Status {
  uid: number;
  title: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
