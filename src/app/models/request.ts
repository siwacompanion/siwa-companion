import {User} from './user';
import {Deserializable} from './deserializable';

export class Request implements Deserializable {
  uid: number;
  status: number;
  subject: string;
  message: string;
  sender: User;
  receiver: User;
  tstamp: number;

  deserialize(input: any) {
    Object.assign(this, input);
    if (input['sender']) {
      this.sender = new User().deserialize(input['sender']);
    }
    if (input['receiver']) {
      this.receiver = new User().deserialize(input['receiver']);
    }
    if (input['tstamp']) {
      this.tstamp = input['tstamp'] * 1000;
    }
    return this;
  }
}
