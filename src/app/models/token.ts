import {Deserializable} from './deserializable';

export class Token implements Deserializable {
  access_token: string;
  refresh_token: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
