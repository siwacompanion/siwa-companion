import {Deserializable} from './deserializable';
import {Status} from './status';

export class User implements Deserializable {
  uid: number;
  firstName: string;
  lastName: string;
  status: Status;
  slackId: string;
  images: Array<string> = [];
  commander: boolean;

  deserialize(input: any) {
    Object.assign(this, input);
    if (input.status) {
      this.status = new Status().deserialize(input.status);
    }
    return this;
  }

  constructor() {
  }

  getFullName() {
    return this.firstName + ' ' + this.lastName;
  }

  getStatusClass() {
    if (this.status) {
      switch (this.status.uid) {
        case 1:
          return 'online';
        default:
          return '';
      }
    } else {
      return '';
    }
  }

  getSlackUrl() {
    return 'slack://user?team=T4K7AH3QB&id=' + this.slackId;
  }
}
