import {User} from './user';
import {Deserializable} from './deserializable';

export class Team implements Deserializable {
  uid: number;
  name: string;
  members: Array<User>;

  deserialize(input: any) {
    Object.assign(this, input);
    this.members = [];
    input.members.forEach((member: User) => {
      this.members.push(new User().deserialize(member));
    });
    return this;
  }
}
