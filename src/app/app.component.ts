import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, ActivationEnd} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {DataService} from './services/data.service';
import {NotificationService} from './services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public activeRequests = 0;
  public subscribed = false;

  constructor(private router: Router, private _titleService: Title, public dataService: DataService, private _notificationService: NotificationService) {
    this.dataService.loadStatuses();
  }

  title = 'app';
  sidebarActive = true;
  sidebarHidden = false;

  ngOnInit() {
    this.subscribeToRequestNotifications();

    this.dataService.loading++;
    this.router.events.subscribe((route: ActivatedRoute) => {
      if (route instanceof ActivationEnd) {
        if (route.snapshot.data) {
          if (route.snapshot.data['title']) {
            this._titleService.setTitle(route.snapshot.data['title']);
          }
          if (typeof route.snapshot.data['sidebarHidden'] !== 'undefined') {
            this.sidebarHidden = route.snapshot.data['sidebarHidden'];
          } else {
            this.sidebarHidden = false;
          }
        }
      }
    });
    this.dataService.reloadMe().then(
      () => {
        this.dataService.loading--;
      },
      error => {
        this.dataService.loading--;
      }
    );
  }
  subscribeToRequestNotifications(): void {
    if (this.subscribed) {
      return;
    }

    this._notificationService.onRequestStarted.subscribe(() => {
      this.activeRequests++;
    });

    this._notificationService.onRequestCompleted.subscribe(() => {
      this.activeRequests--;
    });

    this._notificationService.onError.subscribe(data => {
      this.activeRequests--;
    });

    this.subscribed = true;
  }
}
